﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Security.Cryptography;

namespace SharedObjects
{
    [DataContract]
    public abstract class SharedResource
    {
        #region Public Methods
        [DataMember]
        public Int32 Id { get; set; }
        [DataMember]
        public byte[] DigitalSignature { get; set; }

        #endregion
    }
}
