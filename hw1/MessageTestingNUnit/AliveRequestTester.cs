﻿using System;
using System.Text;
using NUnit.Framework;

using Messages;

namespace MessageTesting
{
	[TestFixture]
	public class AliveRequestTester
	{
		[Test]
		public void AliveRequest_TestEverything()
		{
			AliveRequest r1 = new AliveRequest();
			Assert.IsNotNull(r1);

			byte[] bytes = r1.Encode();

			string tmp = ASCIIEncoding.ASCII.GetString(bytes);
			Assert.AreEqual("{\"__type\":\"AliveRequest:#Messages\"}", tmp);

			Message m2 = Message.Decode(bytes);
			AliveRequest r3 = m2 as AliveRequest;
			Assert.IsNotNull(r3);
		}
	}
}

