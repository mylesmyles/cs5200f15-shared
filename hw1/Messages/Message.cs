﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

using SharedObjects;

namespace Messages
{
    [DataContract]
    public class Message
    {
        private static List<Type> serializableTypes = new List<Type>()
        {
            typeof(AliveRequest),
            typeof(GameListRequest),
            typeof(GameListReply),
            typeof(JoinGameReply),
            typeof(JoinGameRequest),
            typeof(LoginRequest),
            typeof(LoginReply),
            typeof(LogoutRequest),
            typeof(Reply),
            typeof(Request)
        };

        public Message()
        {
        }

        public byte[] Encode()
        {
//			DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings();
//			settings.KnownTypes = serializableTypes;
//			settings.EmitTypeInformation = EmitTypeInformation.Always;
//            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Message), settings);

			DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Message), serializableTypes, Int32.MaxValue, false, null, true);

            MemoryStream mstream = new MemoryStream();
            serializer.WriteObject(mstream, this);

            return mstream.ToArray();
        }

        public static Message Decode(byte[] bytes)
        {

            MemoryStream mstream = new MemoryStream(bytes);
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Message), serializableTypes);
            Message result = (Message)serializer.ReadObject(mstream);

            return result;
        }

    }
}
