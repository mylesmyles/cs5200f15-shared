﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.RequestMessages
{
    [DataContract]
    public class FillBalloon
    {
        [DataMember]
        public Balloon Balloon { get; set; }
        [DataMember]
        public Penny[] Pennies { get; set; }
    }
}
