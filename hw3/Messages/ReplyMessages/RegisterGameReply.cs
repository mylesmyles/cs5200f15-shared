﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages
{
    [DataContract]
    public class RegisterGameReply : Reply
    {
        [DataMember]
        public GameInfo Game { get; set; }        
    }
}
