﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.StatusMessages
{
    [DataContract]
    public class EndGame : StatusMessage
    {
        // TODO: Include information about winners, ties, and losers (maybe)
    }
}
