﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

using log4net;

using CommSub;
using Messages;
using SharedObjects;
using Utils;

namespace CommSub
{
    public abstract class CommProcess : BackgroundThread
    {
        #region Private Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(CommProcess));
        private static readonly ILog logDeep = LogManager.GetLogger(typeof(CommProcess).ToString() + "_Deep");

        protected const int mainLoopSleep = 200;

        private CommSubsystem commSubsystem; 
        protected CommProcessState myState;

        protected int inProcessOfStayingAlive = 0;
        #endregion

        #region Public Properties
        public RuntimeOptions Options { get; set; }
        #endregion

        #region Convenient Accessors
        public CommSubsystem CommSubsystem { get { return commSubsystem; } }
        public CommProcessState MyState { get { return myState; } }
        public bool IsInitialized { get { return myState != null && myState.MyProcessInfo!=null && myState.MyProcessInfo.Status != ProcessInfo.StatusCode.NotInitialized; } }
        public Communicator MyCommunicator { get { return (MyState == null) ? null : CommSubsystem.Communicator; } }
        public Listener MyListener { get { return (MyState == null) ? null : CommSubsystem.Listener; } }
        public Doer MyDoer { get { return (MyState == null) ? null : CommSubsystem.Doer; } }
        #endregion

        #region Constructors, Initializers, Destructors
        public virtual void SetupCommSubsystem(ConversationFactory conversationFactory)
        {
            commSubsystem = new CommSubsystem() { ConversationFactory = conversationFactory, MaxPort = Options.MaxPort, MinPort = Options.MinPort };
            commSubsystem.Initialize();
            commSubsystem.Start();
        }

        public override void Stop()
        {
            log.DebugFormat("Entering Stop, with Status={0}", (MyState.MyProcessInfo==null) ? "null" : MyState.MyProcessInfo.Status.ToString());
            if (MyState.MyProcessInfo!=null)
                MyState.MyProcessInfo.Status = ProcessInfo.StatusCode.Terminating;

            log.DebugFormat("Status={0}", (MyState.MyProcessInfo == null) ? "null" : MyState.MyProcessInfo.Status.ToString());

            base.Stop();
            CommSubsystem.Stop();
            Cleanup();

            if (MyState.MyProcessInfo != null)
                MyState.MyProcessInfo.Status = ProcessInfo.StatusCode.Unknown;

            log.DebugFormat("Leaving Stop, with Status={0}", (MyState.MyProcessInfo == null) ? "null" : MyState.MyProcessInfo.Status.ToString());
        }

        public virtual void Cleanup()
        {
            myState.MyProcessInfo.Status = ProcessInfo.StatusCode.NotInitialized;
        }

        #endregion

        #region Other Public Methods 

        public void WaitToCloseDown(int timeout)
        {
            log.Debug("Enter WaitToCloseDown");
            while (timeout > 0 && IsInitialized)
            {
                Thread.Sleep(500);
                timeout -= 500;
                if (IsInitialized)
                    log.DebugFormat("Waiting for another {0} ms", timeout);
                else
                    log.Debug("Shutdown complete");
            }
        }

        #endregion
    }
}
