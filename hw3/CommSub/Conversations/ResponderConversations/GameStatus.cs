﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;

using CommSub;
using CommSub.Conversations;
using Messages;
using Messages.StatusMessages;
using Messages.StreamMessages;
using SharedObjects;
using Utils;

using log4net;
namespace CommSub.Conversations.ResponderConversations
{
    public abstract class GameStatus : Conversation
    {
        #region Private Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(GameStatus));
        protected static Type[] allowedTypes = new Type[] { typeof(ReadyToStart) };
        protected static byte[] EOL = new byte[] { 10 };

        protected EnvelopeQueue queue = null;
        protected TcpClient tcpClient = null;
        protected bool keepGoing;
        protected int remoteProcessId;

        protected ConcurrentQueue<StreamMessage> messagesToSend = new ConcurrentQueue<StreamMessage>();
        protected ManualResetEvent messagesToSendEvent = new ManualResetEvent(false);

        #endregion

        public override void Execute(object context)
        {
            log.Debug("In Execute");
            Done = false;

            if (IsEnvelopeValid(IncomingEnvelope, false, true, typeof(ReadyToStart)))
            {
                log.DebugFormat("Start a GameStatus conversation with EP={0}", IncomingEnvelope.EP);

                queue = CommSubsystem.QueueDictionary.CreateQueue(IncomingEnvelope.Message.ConversationId);

                SetupEventHandlers();


                if ((error = ProcessReadyToStartMessage(IncomingEnvelope.Message as ReadyToStart)) == null)
                {
                    if (!SetupTcpClient())
                        error = new Error() { Message = "Cannot setup a TCP Client for a GameStatus conversation" };
                    else if (!ReadStreamMessages())
                        error = new Error() { Message = string.Format("Unexpected error while streaming messages in a GameStatus conversation with {0}", IncomingEnvelope.EP) };
                }

                TakeDownEventHandlers();

            }

            if (tcpClient != null)
            {
                log.Debug("Close the tcp connection");
                tcpClient.Close();
            }

            if (error != null)
            {
                ProcessState.AddError(error);
                log.Warn(error.Message);
            }

            Done = true;
            log.Debug("End GameStatus Conversation");

        }

        private bool SetupTcpClient()
        {
            bool result = false;
            ReadyToStart request = IncomingEnvelope.Message as ReadyToStart;
            remoteProcessId = request.MessageNr.ProcessId;

            try
            {
                log.DebugFormat("Try to connect to {0}:{1}", request.StreamEP.Host, request.StreamEP.Port);
                tcpClient = new TcpClient(request.StreamEP.Host, request.StreamEP.Port);
                result = tcpClient.Connected;
                log.DebugFormat("Is tcpClient Connected = {0}", result);
                keepGoing = true;
            }
            catch (Exception err)
            {
                log.Error(err);
            }
            return result;
        }

        private bool ReadStreamMessages()
        {
            log.DebugFormat("In ReadStreamMessages");

            if (tcpClient.Connected)
            {
                log.Debug("tcpClient is still connected");

                QueueUpKeyInfo();

                using (NetworkStream stream = tcpClient.GetStream())
                {
                    stream.ReadTimeout = 100;
                    log.DebugFormat("At top of loop: keepGoing={0}, Type={1}, Status={2}", keepGoing, ProcessState.MyProcessInfo.Type, ProcessState.MyProcessInfo.Status);
                    while (keepGoing && !ProcessState.Quit &&
                                (ProcessState.MyProcessInfo.Type == ProcessInfo.ProcessType.Registry ||
                                 ProcessState.MyProcessInfo.Status == ProcessInfo.StatusCode.JoiningGame ||
                                 ProcessState.MyProcessInfo.Status == ProcessInfo.StatusCode.JoinedGame ||
                                 ProcessState.MyProcessInfo.Status == ProcessInfo.StatusCode.PlayingGame))
                    {
                        try
                        {
                            log.DebugFormat("Inside top of read loop, with queue = {0}", (queue==null) ? "null" : queue.QueueId.ToString());
                            Envelope env = queue.Dequeue(1);
                            log.DebugFormat("Back from dequeue with {0}", (env == null) ? "nothing" : "an envelope");
                            if (env!=null && env.Message!=null && env.Message is EndGame && env.EP == IncomingEnvelope.EP)
                            {
                                log.DebugFormat("Received EndGame messsage");
                                ProcessEndGameMessage(env.Message as EndGame);
                                keepGoing = false;
                            }

                            log.DebugFormat("Stream can write = {0}", stream.CanWrite);
                            if (stream.CanWrite)
                            {
                                StreamMessage message;
                                if (messagesToSend.TryDequeue(out message))
                                {
                                    log.DebugFormat("Send out {0}", message.GetType());
                                    if (!stream.WriteStreamMessage(message))
                                    {
                                        keepGoing = false;
                                        log.Warn("TCP Stream write failed -- Terminating GameStatus conversation");
                                    }
                                }
                            }

                            log.DebugFormat("Stream can read = {0}", stream.CanRead);
                            if (stream.CanRead)
                            {
                                log.DebugFormat("Try to read from the stream with a timeout of {0}", stream.ReadTimeout);
                                StreamMessage message = stream.ReadStreamMessage();
                                if (message != null)
                                {
                                    log.DebugFormat("Received a {0} message", message.GetType());
                                    ProcessIncomingStreamMessage(message);
                                }
                                else
                                    log.Debug("No stream message read");
                            }
                            else
                            {
                                log.Debug("Can no longer read from stream -- terminate conversation");
                                keepGoing = false;
                            }
                        }
                        catch (Exception err)
                        {
                            ProcessState.AddError(new Error() { Message = err.Message });
                            log.Warn(err.Message);
                            keepGoing = false;
                        }
                        log.DebugFormat("Bottom of ReadStreamMessage loop, with keepGoing={0} and status={1}", keepGoing, ProcessState.MyProcessInfo.Status);
                    }
                }
            }
            else
            {
                log.Debug("tcpClient is not connected -- Cannot get stream");
                keepGoing = false;
            }

            return keepGoing;
        }

        protected void ProcessIncomingStreamMessage(StreamMessage message)
        {
            log.DebugFormat("Process incoming stream message of type = {0}", message.GetType());
            if (message is InGame)
                ProcessInGameMessage(message as InGame);
            else if (message is NotInGame)
                ProcessNotInGameMessasge(message as NotInGame);
            else if (message is StartGame)
                ProcessStartGameMessage(message as StartGame);
            else if (message is HaveAPenny)
                ProcessHaveAPennyMessage(message as HaveAPenny);
            else if (message is PennyUsed)
                ProcessPennyUsedMessage(message as PennyUsed);
            else if (message is KeyInfo)
                ProcessKeyInfoMessage(message as KeyInfo);                
        }

        private void QueueUpKeyInfo()
        {
            log.Debug("Queue up KeyInfo message");
            RSAParameters rsaParameters = ProcessState.MyRSA.ExportParameters(false);
            KeyInfo keyInfo = new KeyInfo()
            {
                ProcessId = ProcessState.MyProcessInfo.ProcessId,
                PublicKeyExponent = rsaParameters.Exponent,
                PublicKeyModulus = rsaParameters.Modulus
            };
            messagesToSend.Enqueue(keyInfo);
            log.DebugFormat("Exponent={0}", Utils.HelperFunctions.ByteToStringDisplay(keyInfo.PublicKeyExponent));
            log.DebugFormat("Modulus={0}", Utils.HelperFunctions.ByteToStringDisplay(keyInfo.PublicKeyModulus));
        }

        protected abstract Error ProcessReadyToStartMessage(ReadyToStart message);

        protected abstract void ProcessInGameMessage(InGame message);

        protected abstract void ProcessNotInGameMessasge(NotInGame message);

        protected abstract void ProcessStartGameMessage(StartGame message);

        protected abstract void ProcessEndGameMessage(EndGame message);

        protected virtual void ProcessHaveAPennyMessage(HaveAPenny message) { }

        protected virtual void ProcessPennyUsedMessage(PennyUsed message) { }

        protected virtual void ProcessKeyInfoMessage(KeyInfo message) { }

        protected virtual void SetupEventHandlers() { }

        protected virtual void TakeDownEventHandlers() { }

    }
}
